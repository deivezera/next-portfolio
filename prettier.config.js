module.exports = {
  semi: false,
  singleQuote: true,
  arrowParens: 'avoids',
  trailingComma: 'none',
  endOfLine: 'auto'
}
