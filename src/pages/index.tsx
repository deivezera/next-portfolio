import React from 'react'
import { Container } from '../styles/pages/Home'
import BackgroundSVG from '../assets/backgroundSVG.svg'

const Home: React.FC = () => {
  return (
    <Container>
      <BackgroundSVG />
      <h1>Em construção</h1>
      <p>Portfolio de desenvolvimento de Davi Szeremeta Zabroski</p>
    </Container>
  )
}
export default Home
