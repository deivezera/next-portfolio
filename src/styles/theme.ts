const theme = {
  colors: {
    background: '#121214',
    text: '#e1e1e6',
    primary: '#28df99'
  }
}
export default theme
